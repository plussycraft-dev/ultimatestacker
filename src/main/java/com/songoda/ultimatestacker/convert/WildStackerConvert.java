package com.songoda.ultimatestacker.convert;

import com.bgsoftware.wildstacker.WildStackerPlugin;
import com.bgsoftware.wildstacker.api.WildStackerAPI;
import com.songoda.ultimatestacker.UltimateStacker;
import com.songoda.ultimatestacker.entity.EntityStackManager;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;

public class WildStackerConvert implements Convert {

    private final UltimateStacker plugin;

    public WildStackerConvert(UltimateStacker plugin) {
        this.plugin = plugin;
    }

    @Override
    public String getName() {
        return "WildStacker";
    }

    @Override
    public boolean canEntities() {
        return true;
    }

    @Override
    public boolean canSpawners() {
        return true;
    }

    @Override
    public void convertEntities() {
        EntityStackManager entityStackManager = plugin.getEntityStackManager();
        for (World world : Bukkit.getWorlds()) {
            for (Entity entity : world.getEntities()) {
                if (!(entity instanceof LivingEntity)) continue;
                if (!entityStackManager.isStacked(entity)) {
                    entityStackManager
                            .addStack(entity, WildStackerAPI.getEntityAmount((LivingEntity) entity));
                    continue;
                }
                entityStackManager
                        .getStack(entity).setAmount(WildStackerAPI.getEntityAmount((LivingEntity) entity));
            }
        }

    }

    @Override
    public void convertSpawners() {

    }

    @Override
    public void disablePlugin() {
        Bukkit.getPluginManager().disablePlugin(WildStackerPlugin.getPlugin());
    }
}
