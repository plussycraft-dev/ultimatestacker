package com.songoda.ultimatestacker.entity;

public enum Split {

    NAME_TAG, MUSHROOM_SHEAR, SHEEP_SHEAR, SHEEP_DYE, ENTITY_BREED

}
